# SaveUp

## Examples of creating and using Retrofit

* GET request

```java
Retrofit retrofit = RetrofitFactory.getRetrofit();
UserService userService = retrofit.create(UserService.class);
Call<User> call = userService.getUser(1);
call.enqueue(new Callback<User>() {
    @Override
    public void onResponse(Call<User> call, Response<User> response) {
        System.out.println(String.format("Returned user is: %s", response.body().toString()));
    }

    @Override
    public void onFailure(Call<User> call, Throwable t) {
        t.printStackTrace();
    }
});
```

* POST request

```java
Retrofit retrofit = RetrofitFactory.getRetrofit();
UserService userService = retrofit.create(UserService.class);
User newUser = new User();
newUser.setEmail("john.doe@example.com");
newUser.setFirstName("John");
newUser.setLastName("Doe");
Call<User> call = userService.createUser(newUser);
call.enqueue(new Callback<User>() {
    @Override
    public void onResponse(Call<User> call, Response<User> response) {
        System.out.println(String.format("Created user is: %s", response.body().toString()));
    }

    @Override
    public void onFailure(Call<User> call, Throwable t) {
        t.printStackTrace();
    }
});
```
