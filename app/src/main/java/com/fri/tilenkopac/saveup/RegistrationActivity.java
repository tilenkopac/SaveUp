package com.fri.tilenkopac.saveup;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.fri.tilenkopac.saveup.rest.RetrofitFactory;
import com.fri.tilenkopac.saveup.rest.entities.User;
import com.fri.tilenkopac.saveup.rest.services.UserService;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegistrationActivity extends AppCompatActivity {

    private EditText firstNameEditText;
    private EditText lastNameEditText;
    private EditText emailEditText;
    private EditText passwordEditText;
    private Button registerButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);

        firstNameEditText = findViewById(R.id.firstNameEditText);
        lastNameEditText = findViewById(R.id.lastNameEditText);
        emailEditText = findViewById(R.id.emailEditText);
        passwordEditText = findViewById(R.id.passwordEditText);
        registerButton = findViewById(R.id.registerButton);

        registerButton.setOnClickListener(v -> {
            final User newUser = new User();
            newUser.setFirstName(firstNameEditText.getText().toString());
            newUser.setLastName(lastNameEditText.getText().toString());
            newUser.setEmail(emailEditText.getText().toString());
            newUser.setPassword(passwordEditText.getText().toString());

            final UserService userService = RetrofitFactory.getRetrofit().create(UserService.class);
            final SharedPreferences sharedPreferences = getSharedPreferences(SaveUpApp.SHARED_PREFERENCES_NAME, MODE_PRIVATE);
            final SharedPreferences.Editor editor = sharedPreferences.edit();
            final int userId = sharedPreferences.getInt(SaveUpApp.USER_ID_KEY, 0);
            final boolean userLoggedIn = sharedPreferences.getBoolean(SaveUpApp.USER_LOGGED_IN_KEY, true);

            // create new user instead of updating the current one if user is logged in
            if (!userLoggedIn) {
                final Call<User> call = userService.updateUser(userId, newUser);
                call.enqueue(new Callback<User>() {
                    @Override
                    public void onResponse(Call<User> call, Response<User> response) {
                        if (response.code() == 200) {
                            editor.putInt(SaveUpApp.USER_ID_KEY, response.body().getId());
                            editor.putBoolean(SaveUpApp.USER_LOGGED_IN_KEY, true);
                            editor.apply();
                            redirectToMainActivity();
                        } else {
                            makeRegistrationFailedToast();
                        }
                    }

                    @Override
                    public void onFailure(Call<User> call, Throwable t) {
                        t.printStackTrace();
                    }
                });
            } else {
                final Call<User> call = userService.createUser(newUser);
                call.enqueue(new Callback<User>() {
                    @Override
                    public void onResponse(Call<User> call, Response<User> response) {
                        if (response.code() == 200) {
                            editor.putInt(SaveUpApp.USER_ID_KEY, response.body().getId());
                            editor.apply();
                            redirectToMainActivity();
                        } else {
                            makeRegistrationFailedToast();
                        }
                    }

                    @Override
                    public void onFailure(Call<User> call, Throwable t) {
                        t.printStackTrace();
                    }
                });
            }
        });
    }

    private void makeRegistrationFailedToast() {
        Toast.makeText(this, "Registration failed", Toast.LENGTH_LONG);
    }

    private void redirectToMainActivity() {
        final Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

}