package com.fri.tilenkopac.saveup.rest.services;

import com.fri.tilenkopac.saveup.rest.entities.Budget;
import com.fri.tilenkopac.saveup.rest.entities.CustomCategory;
import com.fri.tilenkopac.saveup.rest.entities.Reminder;
import com.fri.tilenkopac.saveup.rest.entities.Transaction;
import com.fri.tilenkopac.saveup.rest.entities.User;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface UserService {

    @GET("users/{user_id}")
    Call<User> getUser(@Path("user_id") int userId);

    @GET("users/{user_id}/transactions")
    Call<List<Transaction>> getUserTransactions(@Path("user_id") int userId);

    @GET("users/{user_id}/categories")
    Call<List<CustomCategory>> getUserCategories(@Path("user_id") int userId);

    @GET("users/{user_id}/budgets")
    Call<List<Budget>> getUserBudgets(@Path("user_id") int userId);

    @GET("users/{user_id}/reminders")
    Call<List<Reminder>> getUserReminders(@Path("user_id") int userId);

    @POST("users/create")
    Call<User> createUser(@Body User user);

    @POST("users/{user_id}/transactions/create")
    Call<Transaction> createTransaction(@Path("user_id") int userId, @Body Transaction transaction);

    @POST("users/{user_id}/categories/create")
    Call<CustomCategory> createCustomCategory(@Path("user_id") int userId, @Body CustomCategory customCategory);

    @POST("users/{user_id}/budgets/create")
    Call<Budget> createBudget(@Path("user_id") int userId, @Body Budget budget);

    @POST("users/{user_id}/reminders/create")
    Call<Reminder> createReminder(@Path("user_id") int userId, @Body Reminder reminder);

    @PUT("users/{user_id}/register")
    Call<User> updateUser(@Path("user_id") int userId, @Body User user);

    @DELETE("users/{user_id}/reminders/delete")
    Call<Void> deleteUserReminder(@Path("user_id") int userId);

}
