package com.fri.tilenkopac.saveup;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


/**
 * A simple {@link Fragment} subclass.
 */
public class TransactionDetailsFragment extends Fragment {

    SendMessage SM;
    public TransactionDetailsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment


        super.onCreate(savedInstanceState);
        Bundle extras = getArguments();
        View view = inflater.inflate(R.layout.fragment_transaction_details, container, false);


        TextView type = (TextView) view.findViewById(R.id.infoTypeText);
        TextView amount = (TextView) view.findViewById(R.id.infoAmountText);
        TextView category = (TextView) view.findViewById(R.id.infoCategoryText);
        //TextView location = (TextView) view.findViewById(R.id.infoLocationText);
        TextView description = (TextView) view.findViewById(R.id.infoDescriptionText);
        TextView dateAndTime = (TextView) view.findViewById(R.id.infoDateAndTimeText);

        if(extras != null){
            type.setText(extras.getString("type","err"));
            amount.setText(extras.getString("amount","err"));
            category.setText(extras.getString("category","err"));
            //location.setText(extras.getString("location","err"));
            description.setText(extras.getString("description","err"));
            dateAndTime.setText(extras.getString("dateAndTime","err"));
            SM.sendData(extras.getInt("id"));
        }



        return view;//inflater.inflate(R.layout.fragment_transaction_details, container, false);
    }

    interface SendMessage {
        void sendData(int message);
    }
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        try {
            SM = (SendMessage) getActivity();
        } catch (ClassCastException e) {
            throw new ClassCastException("Error in retrieving data. Please try again");
        }
    }

}
