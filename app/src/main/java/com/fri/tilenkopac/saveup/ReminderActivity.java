package com.fri.tilenkopac.saveup;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TimePicker;
import android.widget.Toast;

import com.fri.tilenkopac.saveup.rest.RetrofitFactory;
import com.fri.tilenkopac.saveup.rest.entities.Reminder;
import com.fri.tilenkopac.saveup.rest.entities.User;
import com.fri.tilenkopac.saveup.rest.services.ReminderService;
import com.fri.tilenkopac.saveup.rest.services.UserService;

import java.io.IOException;
import java.sql.Time;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class ReminderActivity extends AppCompatActivity {

    private TimePicker timePicker;
    private Button buttonCreateReminder;
    private Switch switchReminderOnOff;
    private Button buttonBackToMainActivity;





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reminder);


        switchReminderOnOff = (Switch) findViewById(R.id.switchReminderOnOff);
        timePicker = (TimePicker) findViewById(R.id.timePicker);
        timePicker.setOnTimeChangedListener(new TimePicker.OnTimeChangedListener() {
            @Override
            public void onTimeChanged(TimePicker timePicker, int i, int i1) {
                switchReminderOnOff.setChecked(true);
            }
        });


        buttonCreateReminder = (Button) findViewById(R.id.buttonCreateReminder);
        buttonCreateReminder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (switchReminderOnOff.isChecked()){
                    int hour = timePicker.getHour();
                    int min = timePicker.getMinute();
                    SimpleDateFormat formatter = new SimpleDateFormat("hh:mm");
                    String dateInString = hour+":"+min;
                    try {
                        Date date = formatter.parse(dateInString);
                        Time t = new Time(date.getTime());
                        setReminderOn(t);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    switchToMainActivity();
                }
                else {
                    Toast.makeText(getReminderActivityContext(), "Turn the switch ON.", Toast.LENGTH_LONG).show();
                }
            }
        });

        buttonBackToMainActivity = (Button) findViewById(R.id.buttonBackToMainActivity);
        buttonBackToMainActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switchToMainActivity();
            }
        });
    }


    private void setReminderOn(Time time){
        Retrofit retrofit = RetrofitFactory.getRetrofit();
        UserService userService = retrofit.create(UserService.class);

        Reminder newReminder = new Reminder();
        newReminder.setTime(time);

        SharedPreferences sharedPreferences = getSharedPreferences(SaveUpApp.SHARED_PREFERENCES_NAME, MODE_PRIVATE);
        int userId = sharedPreferences.getInt(SaveUpApp.USER_ID_KEY,1);
        userId = 1;

        Call<Reminder> call = userService.createReminder(userId, newReminder);
        call.enqueue(new Callback<Reminder>() {
            @Override
            public void onResponse(Call<Reminder> call, Response<Reminder> response) {
                System.out.println(response.body().toString());

                if (response.code() == 400) {
                    try {
                        Log.v("Error code 400",response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<Reminder> call, Throwable t) {

            }
        });

    }

    private Context getReminderActivityContext(){
        return this;
    }
    private void switchToMainActivity(){
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

}
