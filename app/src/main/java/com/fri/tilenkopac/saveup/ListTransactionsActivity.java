package com.fri.tilenkopac.saveup;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.fri.tilenkopac.saveup.rest.RetrofitFactory;
import com.fri.tilenkopac.saveup.rest.entities.Transaction;
import com.fri.tilenkopac.saveup.rest.entities.User;
import com.fri.tilenkopac.saveup.rest.services.UserService;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class ListTransactionsActivity extends AppCompatActivity {

    Retrofit retrofit = RetrofitFactory.getRetrofit();
    UserService userService = retrofit.create(UserService.class);
    int userId;
    Call<List<Transaction>> call;
    User user;
    List<Transaction> listTrans;

    private LinearLayout lv;

    //TODO težave z pridobitvijo seznam transakcij
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_transactions);
        lv = (LinearLayout) findViewById(R.id.listLayout);
        userId = getSharedPreferences(SaveUpApp.SHARED_PREFERENCES_NAME, MODE_PRIVATE).getInt(SaveUpApp.USER_ID_KEY,1);
        call = userService.getUserTransactions(userId);

        getRequest(call);



    }

    public void goToLogin (View view){
        Intent intent = new Intent (this, LoginActivity.class);
        startActivity(intent);
    }

    public void goToInfo(View view,int i){
        System.out.println(listTrans.get(i).toString());
        Intent intent = new Intent (this, TransactionInfoActivity.class);
        intent.putExtra("type",listTrans.get(i).getType() != null ? listTrans.get(i).getType() : "expense");
        intent.putExtra("amount",listTrans.get(i).getValue() != 0 ? Float.toString(listTrans.get(i).getValue()) : "0");
        intent.putExtra("category",listTrans.get(i).getCategory() != null ? listTrans.get(i).getCategory().getName() : "Job");
        intent.putExtra("location",listTrans.get(i).getLocation() != null ? listTrans.get(i).getLocation().toString() : "Ljubljana");
        intent.putExtra("description",listTrans.get(i).getDescription() != null ? listTrans.get(i).getDescription() : "/");
        intent.putExtra("dateAndTime",listTrans.get(i).getDateAndTime() != null ? listTrans.get(i).getDateAndTime().toString() : "1547414565");
        intent.putExtra("id", listTrans.get(i).getId() != 0 ? listTrans.get(i).getId() : "1");

        startActivity(intent);
    }

    void getRequest(Call<List<Transaction>> call){
        call.enqueue(new Callback<List<Transaction>>() {
            @Override
            public void onResponse(Call<List<Transaction>> call, Response<List<Transaction>> response) {
                System.out.println(String.format("Returned user is: %s", response.body().toString()));
                    listTrans = response.body();

                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);

                if(listTrans != null){
                    // reverse transaction list so that new transactions are at the beginning of it
                    Collections.reverse(listTrans);
                    for(int j=0;j<listTrans.size();j++)
                    {
                        // Create LinearLayout
                        LinearLayout ll = new LinearLayout(getListTransactionActivityContex());
                        ll.setOrientation(LinearLayout.HORIZONTAL);
                        if(listTrans.get(j).getType().equals("income")){
                            ll.setBackgroundColor(ContextCompat.getColor(getBaseContext(),R.color.BGgreen));
                        }else{
                            ll.setBackgroundColor(ContextCompat.getColor(getBaseContext(),R.color.colorRed));
                        }

                        // Create TextView type of transaction
                        TextView type = new TextView(getListTransactionActivityContex());
                        type.setText(listTrans.get(j).getType());
                        type.setTextSize(24);
                        type.setWidth(400);
                        type.setHeight(400);
                        type.setTypeface(ResourcesCompat.getFont(getListTransactionActivityContex(), R.font.ubuntu_light));
                        ll.addView(type);

                        // Create TextView value of transaction
                        TextView value = new TextView(getListTransactionActivityContex());
                        value.setText(Float.toString(listTrans.get(j).getValue()));
                        value.setTextSize(24);
                        value.setWidth(400);
                        value.setTypeface(ResourcesCompat.getFont(getListTransactionActivityContex(), R.font.ubuntu_light));
                        ll.addView(value);

                        // Create Button for more info
                        final Button btn = new Button(getListTransactionActivityContex());
                        // Give button an ID
                        btn.setId(j+1);
                        btn.setText("More Info");
                        btn.setTypeface(ResourcesCompat.getFont(getListTransactionActivityContex(), R.font.ubuntu_light));
                        // set the layoutParams on the button
                        btn.setLayoutParams(params);

                        final int index = j;
                        // Set click listener for button
                        btn.setOnClickListener(new View.OnClickListener() {
                            public void onClick(View v) {
                                goToInfo(v,index);
                            }
                        });

                        //Add button to LinearLayout
                        ll.addView(btn);
                        //Add button to LinearLayout defined in XML
                        lv.addView(ll);
                    }
                }



            }

            @Override
            public void onFailure(Call<List<Transaction>> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    private Context getListTransactionActivityContex(){
        return this;
    }

}
