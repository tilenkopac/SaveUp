package com.fri.tilenkopac.saveup;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

public class InfoTransactionActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info_transaction);
        Bundle extras = getIntent().getExtras();
        //stari activity---prestavljen v TransactionDetailFragment
        /*
        TextView type = (TextView) findViewById(R.id.infoTypeText);
        TextView amount = (TextView) findViewById(R.id.infoAmountText);
        TextView category = (TextView) findViewById(R.id.infoCategoryText);
        TextView location = (TextView) findViewById(R.id.infoLocationText);
        TextView description = (TextView) findViewById(R.id.infoDescriptionText);
        TextView dateAndTime = (TextView) findViewById(R.id.infoDateAndTimeText);

        if (extras != null) {
            type.setText(extras.getString("type", "err"));
            amount.setText(extras.getString("amount", "err"));
            category.setText(extras.getString("category", "err"));
            location.setText(extras.getString("location", "err"));
            description.setText(extras.getString("description", "err"));
            dateAndTime.setText(extras.getString("dateAndTime", "err"));
        }
        */
    }

    public void goToLogin(View view) {
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
    }
}
