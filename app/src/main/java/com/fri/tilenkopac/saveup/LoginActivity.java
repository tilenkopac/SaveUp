package com.fri.tilenkopac.saveup;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.fri.tilenkopac.saveup.rest.RetrofitFactory;
import com.fri.tilenkopac.saveup.rest.entities.User;
import com.fri.tilenkopac.saveup.rest.services.LoginService;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {

    private EditText emailEditText;
    private EditText passwordEditText;
    private Button loginButton;
    private Button registerButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        emailEditText = findViewById(R.id.emailEditText);
        passwordEditText = findViewById(R.id.passwordEditText);
        loginButton = findViewById(R.id.loginButton);
        registerButton = findViewById(R.id.registerButton);

        loginButton.setOnClickListener(v -> loginUser(emailEditText.getText().toString(), passwordEditText.getText().toString()));
        registerButton.setOnClickListener(v -> redirectToRegistrationActivity());
    }

    private void loginUser(String email, String password) {
        final LoginService loginService = RetrofitFactory.getRetrofit().create(LoginService.class);
        final User userLoggingIn = new User();
        userLoggingIn.setEmail(email);
        userLoggingIn.setPassword(password);
        final Call<User> call = loginService.loginWithEmail(userLoggingIn);
        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                if (response.code() == 200) {
                    final SharedPreferences.Editor editor = getSharedPreferences(SaveUpApp.SHARED_PREFERENCES_NAME, MODE_PRIVATE).edit();
                    editor.putInt(SaveUpApp.USER_ID_KEY, response.body().getId());
                    editor.apply();
                    redirectToMainActivity();
                } else {
                    makeLoginFailedToast();
                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    private void makeLoginFailedToast() {
        Toast.makeText(this, "Login failed", Toast.LENGTH_LONG).show();
    }

    private void redirectToRegistrationActivity() {
        final Intent intent = new Intent(this, RegistrationActivity.class);
        startActivity(intent);
    }

    private void redirectToMainActivity() {
        final Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

}
