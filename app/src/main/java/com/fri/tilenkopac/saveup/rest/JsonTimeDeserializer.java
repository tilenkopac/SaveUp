package com.fri.tilenkopac.saveup.rest;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;
import java.sql.Time;
import java.text.ParseException;
import java.text.SimpleDateFormat;

public class JsonTimeDeserializer implements JsonDeserializer<Time> {

    private static final String TIME_FORMAT = "HH:mm:ss";

    @Override
    public Time deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        try {
            String s = json.getAsString();
            SimpleDateFormat sdf = new SimpleDateFormat(TIME_FORMAT);
            sdf.parse(s);
            long ms = sdf.parse(s).getTime();
            return new Time(ms);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        throw new JsonParseException("Unparseable time: \"" + json.getAsString()
                + "\". Supported formats: " + TIME_FORMAT);
    }

}
