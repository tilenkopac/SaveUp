package com.fri.tilenkopac.saveup.rest.services;

import com.fri.tilenkopac.saveup.rest.entities.Category;
import com.fri.tilenkopac.saveup.rest.entities.PredefinedCategory;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface CategoryService {

    @GET("categories/{category_id}")
    Call<Category> getCategory(@Path("category_id") int categoryId);

    @POST("categories/create")
    Call<PredefinedCategory> createPredefinedCategory(@Body PredefinedCategory predefinedCategory);

}
