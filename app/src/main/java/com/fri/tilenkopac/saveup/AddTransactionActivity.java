package com.fri.tilenkopac.saveup;

import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Address;
import android.location.Geocoder;
import android.support.annotation.ColorRes;
import android.support.annotation.StringRes;
import android.support.constraint.ConstraintLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Layout;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;

import com.fri.tilenkopac.saveup.rest.RetrofitFactory;
import com.fri.tilenkopac.saveup.rest.entities.Category;
import com.fri.tilenkopac.saveup.rest.entities.Location;
import com.fri.tilenkopac.saveup.rest.entities.Transaction;
import com.fri.tilenkopac.saveup.rest.entities.User;
import com.fri.tilenkopac.saveup.rest.services.UserService;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import org.w3c.dom.Text;

import java.io.IOException;
import java.sql.Date;
import java.sql.Time;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

import java.text.DateFormat;


public class AddTransactionActivity extends AppCompatActivity {


    Retrofit retrofit = RetrofitFactory.getRetrofit();
    UserService userService = retrofit.create(UserService.class);
    int userId;
    Transaction transaction = new Transaction();
    User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        userId = getSharedPreferences(SaveUpApp.SHARED_PREFERENCES_NAME, MODE_PRIVATE).getInt(SaveUpApp.USER_ID_KEY,1);
        setContentView(R.layout.activity_add_transaction);
        Spinner spinner = (Spinner) findViewById(R.id.categorySpinner);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.category_array, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);

        Switch switchy = (Switch) findViewById(R.id.typeSwitch);
        TextView texty = (TextView) findViewById(R.id.typeDisplay);
        ConstraintLayout layout = (ConstraintLayout) findViewById(R.id.transLayout);
        if(switchy.isChecked()){
            texty.setText(switchy.getTextOn());
            layout.setBackgroundColor(ContextCompat.getColor(getBaseContext(),R.color.BGgreen));
        }else{
            texty.setText(switchy.getTextOff());
            layout.setBackgroundColor(ContextCompat.getColor(getBaseContext(),R.color.BGred));
        }

    }

    public void goToLogin (View view){
        Intent intent = new Intent (this, LoginActivity.class);
        startActivity(intent);
    }

    public void changeType(View view){
        Switch switchy = (Switch) findViewById(R.id.typeSwitch);
        TextView texty = (TextView) findViewById(R.id.typeDisplay);
        ConstraintLayout layout = (ConstraintLayout) findViewById(R.id.transLayout);
        if(switchy.isChecked()){
            texty.setText(switchy.getTextOn());
            layout.setBackgroundColor(ContextCompat.getColor(getBaseContext(),R.color.BGgreen));
        }else{
            texty.setText(switchy.getTextOff());
            layout.setBackgroundColor(ContextCompat.getColor(getBaseContext(),R.color.BGred));
        }
    }
    //TODO končaj to metodo

    public void finishTransaction(View view){
        TextView transType = (TextView) findViewById(R.id.typeDisplay);
        TextView transDesc = (TextView) findViewById(R.id.transDescrip);
        Spinner transSpinner = (Spinner) findViewById(R.id.categorySpinner);
        EditText transValue = (EditText) findViewById(R.id.transValue);
        TextView transLocation = (TextView) findViewById(R.id.transDescrip2);

        String address = transLocation.getText().toString();
        List<Address> addressList = null;
        Location location = new Location();


        if(!TextUtils.isEmpty(address)){
            Geocoder geocoder = new Geocoder(this);
            try {
                addressList = geocoder.getFromLocationName(address,1);
            } catch (IOException e) {
                e.printStackTrace();
            }

            Address userAddress;
            if (addressList != null && !addressList.isEmpty()) {
                userAddress = addressList.get(0);
                LatLng latLng = new LatLng(userAddress.getLatitude(), userAddress.getLongitude());
                //location.setId(0);
                location.setLatitude((float)latLng.latitude);
                location.setLongitude((float)latLng.longitude);
                location.setAddress(userAddress.getAddressLine(0));
                System.out.println(location.getAddress());
            } else {
                location = null;
            }

        }


        Category category = new Category();
        category.setName(transSpinner.getSelectedItem().toString());

        //TODO date popravi po potrebi če nebo delalo
        /*
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(cal.getTimeInMillis() * 1000L);
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy hh:mm a");
        String date = sdf.format(cal.getTimeInMillis() * 1000L);
        */

        transaction.setType(transType.getText().toString());
        transaction.setDescription(transDesc.getText().toString());
        transaction.setValue(Float.parseFloat(transValue.getText().toString()));
        transaction.setCategory(category);
        transaction.setDateAndTime(new Timestamp(Calendar.getInstance().getTimeInMillis()));
        transaction.setLocation(location);

        Call<Transaction> transy = userService.createTransaction(userId,transaction);
        transy.enqueue(new Callback<Transaction>() {
            @Override
            public void onResponse(Call<Transaction> call, Response<Transaction> response) {
                if (response.code() == 400) {
                    try {
                        Log.v("Error code 400",response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                System.out.println(String.format("Created pony is: %s", response.toString()));
            }

            @Override
            public void onFailure(Call<Transaction> call, Throwable t) {
                t.printStackTrace();
            }
        });

        startActivity(new Intent(this, MainActivity.class));

    }

}
