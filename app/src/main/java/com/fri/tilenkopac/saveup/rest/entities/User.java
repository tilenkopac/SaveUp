package com.fri.tilenkopac.saveup.rest.entities;

import java.util.ArrayList;
import java.util.List;

public class User {

    private int id;
    private String uuid;
    private String email;
    private String password;
    private String firstName;
    private String lastName;
    private List<Transaction> transactions = new ArrayList<>();
    private List<CustomCategory> customCategories = new ArrayList<>();
    private List<Budget> budgets = new ArrayList<>();
    private List<Reminder> reminders = new ArrayList<>();

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public List<Transaction> getTransactions() {
        return transactions;
    }

    public void setTransactions(List<Transaction> transactions) {
        this.transactions = transactions;
    }

    public List<CustomCategory> getCustomCategories() {
        return customCategories;
    }

    public void setCustomCategories(List<CustomCategory> customCategories) {
        this.customCategories = customCategories;
    }

    public List<Budget> getBudgets() {
        return budgets;
    }

    public void setBudgets(List<Budget> budgets) {
        this.budgets = budgets;
    }

    public List<Reminder> getReminders() {
        return reminders;
    }

    public void setReminders(List<Reminder> reminders) {
        this.reminders = reminders;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", uuid='" + uuid + '\'' +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", transactions=" + transactions +
                ", customCategories=" + customCategories +
                ", budgets=" + budgets +
                ", reminders=" + reminders +
                '}';
    }
}
