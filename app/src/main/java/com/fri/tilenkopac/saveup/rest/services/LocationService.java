package com.fri.tilenkopac.saveup.rest.services;

import com.fri.tilenkopac.saveup.rest.entities.Location;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface LocationService {

    @GET("locations/{location_id}")
    Call<Location> getLocation(@Path("location_id") int locationId);

    @POST("locations/create")
    Call<Location> createLocation(@Body Location location);

}
