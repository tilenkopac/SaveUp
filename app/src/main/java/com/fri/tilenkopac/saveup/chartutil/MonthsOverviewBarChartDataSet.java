package com.fri.tilenkopac.saveup.chartutil;

import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;

import java.util.List;

public class MonthsOverviewBarChartDataSet extends BarDataSet {

    public MonthsOverviewBarChartDataSet(List<BarEntry> yVals, String label) {
        super(yVals, label);
    }

    @Override
    public int getColor(int index) {
        // less than 0 red
        if (getEntryForIndex(index).getY() < 0) {
            return mColors.get(0);
        }
        // else green
        else {
            return mColors.get(1);
        }
    }

}
