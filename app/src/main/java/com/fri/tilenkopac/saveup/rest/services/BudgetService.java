package com.fri.tilenkopac.saveup.rest.services;

import com.fri.tilenkopac.saveup.rest.entities.Budget;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface BudgetService {

    @GET("budgets/{budget_id}")
    Call<Budget> getBudget(@Path("budget_id") int budgetId);

}
