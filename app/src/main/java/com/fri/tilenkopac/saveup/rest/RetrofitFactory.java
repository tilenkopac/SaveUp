package com.fri.tilenkopac.saveup.rest;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.sql.Time;
import java.sql.Timestamp;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitFactory {

    private final static String API_PATH = "http://ec2-3-120-126-76.eu-central-1.compute.amazonaws.com:8080/saveuprestapi/api/";

    public static Retrofit getRetrofit() {

        Gson gson = new GsonBuilder()
                .registerTypeAdapter(Timestamp.class, new JsonTimestampDeserializer())
                .registerTypeAdapter(Timestamp.class, new JsonTimestampSerializer())
                .registerTypeAdapter(Time.class, new JsonTimeDeserializer())
                .registerTypeAdapter(Time.class, new JsonTimeSerializer())
                .create();
        return new Retrofit.Builder()
                .baseUrl(API_PATH)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
    }

}
