package com.fri.tilenkopac.saveup;

import android.app.Application;
import android.content.SharedPreferences;

import com.fri.tilenkopac.saveup.rest.RetrofitFactory;
import com.fri.tilenkopac.saveup.rest.entities.User;
import com.fri.tilenkopac.saveup.rest.services.UserService;

import java.util.UUID;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SaveUpApp extends Application {

    public static final String SHARED_PREFERENCES_NAME = "saveup_preferences";
    public static final String FIRST_RUN_KEY = "first_run";
    public static final String USER_LOGGED_IN_KEY = "user_logged_in";
    public static final String USER_ID_KEY = "user_id";

    public SaveUpApp() {
    }

    @Override
    public void onCreate() {
        super.onCreate();

        final SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFERENCES_NAME, MODE_PRIVATE);
        boolean firstRun = sharedPreferences.getBoolean(FIRST_RUN_KEY, true);

        // check if this is a first run after install and if it is, create a new user in the database
        if (firstRun) {
            final SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putBoolean(FIRST_RUN_KEY, false);
            editor.apply();
            final User newUser = new User();
            newUser.setUuid(UUID.randomUUID().toString());
            final UserService userService = RetrofitFactory.getRetrofit().create(UserService.class);
            Call<User> call = userService.createUser(newUser);
            call.enqueue(new Callback<User>() {
                @Override
                public void onResponse(Call<User> call, Response<User> response) {
                    if (response.code() == 200) {
                        // save user id to SharedPreferences for later use
                        int userId = response.body().getId();
                        editor.putInt(USER_ID_KEY, userId);
                        editor.apply();
                    }
                }

                @Override
                public void onFailure(Call<User> call, Throwable t) {
                    t.printStackTrace();
                }
            });
        }
    }

}
