package com.fri.tilenkopac.saveup;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.fri.tilenkopac.saveup.rest.RetrofitFactory;
import com.fri.tilenkopac.saveup.rest.entities.Budget;
import com.fri.tilenkopac.saveup.rest.services.UserService;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;


/**
 * Created by Matevz on 11. 01. 2019.
 */

public class NewBudgetActivity extends Activity {
    private EditText budgetEntry;
    private Button newEntry;
    private Spinner mySpinner;




    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_budget);

        budgetEntry = (EditText) findViewById(R.id.budgetEntry);
        newEntry = (Button) findViewById(R.id.addBudgetEntryButton);
        mySpinner = (Spinner) findViewById(R.id.spinner);


        newEntry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!isEmptyString(budgetEntry.getText().toString())){
                    String budgetInterval = mySpinner.getSelectedItem().toString();

                    System.out.println(budgetEntry.getText());
                    System.out.println(budgetInterval);

                    final SharedPreferences sharedPreferences = getSharedPreferences(SaveUpApp.SHARED_PREFERENCES_NAME, MODE_PRIVATE);
                    int id = sharedPreferences.getInt(SaveUpApp.USER_ID_KEY,1);
                    System.out.println(id);

                    //POST REQUEST
                    Retrofit retrofit = RetrofitFactory.getRetrofit();
                    UserService userService = retrofit.create(UserService.class);
                    Budget budget = new Budget();

                    if(budgetInterval.equals("Daily budget")){
                        budget.setType("daily");
                    }
                    else if(budgetInterval.equals("Weekly budget")){
                        budget.setType("weekly");
                    }
                    else if(budgetInterval.equals("Monthly budget")){
                        budget.setType("monthly");
                    }

                    budget.setSize(Integer.parseInt(budgetEntry.getText().toString()));

                    Call<Budget> call = userService.createBudget(id, budget);
                    call.enqueue(new Callback<Budget>() {
                        @Override
                        public void onResponse(Call<Budget> call, Response<Budget> response) {
                            //System.out.println(String.format("Created budget is: %s", response.body().toString()));
                            System.out.println(response.code() == 200);
                            System.out.println(response.code());
                            Toast.makeText(getBaseContext(), "New "+budgetInterval.toLowerCase()+ " added", Toast.LENGTH_LONG).show();
                        }

                        @Override
                        public void onFailure(Call<Budget> call, Throwable t) {
                            t.printStackTrace();
                        }
                    });


                }else{
                    Toast.makeText(getBaseContext(), "Enter your budget!", Toast.LENGTH_LONG).show();
                    System.out.println("empty string!");
                }

                redirectToBudgetListActivity();
            }
        });


    }

    public static boolean isEmptyString(String text) {
        return (text == null || text.trim().equals("null") || text.trim()
                .length() <= 0);
    }

    private void redirectToBudgetListActivity() {
        startActivity(new Intent(this, BudgetListActivity.class));
    }



}
