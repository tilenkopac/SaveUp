package com.fri.tilenkopac.saveup;

import android.content.Intent;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class TransactionInfoActivity extends AppCompatActivity implements TransactionDetailsFragment.SendMessage{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transaction_info);

        TransactionDetailsFragment transactionDetailsFragment = new TransactionDetailsFragment();
        GoogleMapsFragment googleMapsFragment = new GoogleMapsFragment();
        TransactionDetailsFragment firstFragment = new TransactionDetailsFragment();

        Bundle bundle = getIntent().getExtras();
        firstFragment.setArguments(bundle);
        googleMapsFragment.setArguments(bundle);
        FragmentManager manager = getSupportFragmentManager();
        manager.beginTransaction()
                .replace(R.id.MapFragment, googleMapsFragment, googleMapsFragment.getTag())
                .replace(R.id.DetailFragment,firstFragment,firstFragment.getTag())
                .commit();
    }

    @Override
    public void sendData(int message) {
        GoogleMapsFragment gmf = (GoogleMapsFragment) getSupportFragmentManager().findFragmentById(R.id.MapFragment);
        gmf.displayReceivedData(message);
    }

}
