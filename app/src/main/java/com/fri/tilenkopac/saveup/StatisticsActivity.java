package com.fri.tilenkopac.saveup;

import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import com.fri.tilenkopac.saveup.chartutil.IntToMonthAxisValueFormatter;
import com.fri.tilenkopac.saveup.chartutil.MonthsOverviewBarChartDataSet;
import com.fri.tilenkopac.saveup.rest.RetrofitFactory;
import com.fri.tilenkopac.saveup.rest.entities.Category;
import com.fri.tilenkopac.saveup.rest.entities.Transaction;
import com.fri.tilenkopac.saveup.rest.services.UserService;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarEntry;

import java.time.LocalDate;
import java.time.Period;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class StatisticsActivity extends AppCompatActivity {

    private BarChart monthsOverviewBarChart;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_statistics);

        SharedPreferences sharedPreferences = getSharedPreferences(SaveUpApp.SHARED_PREFERENCES_NAME, MODE_PRIVATE);
        monthsOverviewBarChart = (BarChart) findViewById(R.id.monthsOverview);
        UserService userService = RetrofitFactory.getRetrofit().create(UserService.class);
        int userId = sharedPreferences.getInt(SaveUpApp.USER_ID_KEY, 1);
        Call<List<Transaction>> call = userService.getUserTransactions(userId);
        call.enqueue(new Callback<List<Transaction>>() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onResponse(Call<List<Transaction>> call, Response<List<Transaction>> response) {
                List<BarEntry> entries = new ArrayList<>();
                List<Transaction> transactions = response.body();
                List<Transaction> transactionsThisMonth = new ArrayList<>();

                Calendar currentDateCalendar = Calendar.getInstance();
                Calendar utilCalendar = Calendar.getInstance();
                utilCalendar.setTimeInMillis(transactions.get(0).getDateAndTime().getTime());

                int monthOfPreviousTransaction = utilCalendar.get(Calendar.MONTH);
                int yearOfPreviousTransaction = utilCalendar.get(Calendar.YEAR);
                int xAxisPosition = monthOfPreviousTransaction;
                List<Transaction> transactionsInMonth = new ArrayList<>();
                for (Transaction transaction : transactions) {
                    utilCalendar.setTimeInMillis(transaction.getDateAndTime().getTime());
                    int monthOfCurrentTransaction = utilCalendar.get(Calendar.MONTH);
                    int yearOfCurrentTransaction = utilCalendar.get(Calendar.YEAR);

                    // if transaction has current month and year, add it to list of transactions which happened this month
                    if (monthOfCurrentTransaction == currentDateCalendar.get(Calendar.MONTH) && yearOfCurrentTransaction == currentDateCalendar.get(Calendar.YEAR)) {
                        transactionsThisMonth.add(transaction);
                    }

                    // when changing month, calculate cumulative for previous month and add it as an entry
                    if (monthOfCurrentTransaction == monthOfPreviousTransaction && yearOfCurrentTransaction == yearOfPreviousTransaction) {
                        transactionsInMonth.add(transaction);
                    } else {
                        float monthlyCummulative = calculateMonthlyCumulative(transactionsInMonth);
                        entries.add(new BarEntry(xAxisPosition + 1, monthlyCummulative));
                        transactionsInMonth = new ArrayList<>();
                        transactionsInMonth.add(transaction);

                        xAxisPosition += Period.between(
                                LocalDate.of(yearOfPreviousTransaction, monthOfPreviousTransaction + 1, 1),
                                LocalDate.of(yearOfCurrentTransaction, monthOfCurrentTransaction + 1, 1))
                                .getMonths();
                        monthOfPreviousTransaction = monthOfCurrentTransaction;
                        yearOfPreviousTransaction = yearOfCurrentTransaction;
                    }
                }

                float monthlyCumulative = calculateMonthlyCumulative(transactionsInMonth);
                entries.add(new BarEntry(xAxisPosition + 1, monthlyCumulative));
                drawMonthsOverviewBarChart(entries);
                fillOutStatistics(transactionsThisMonth);
            }

            @Override
            public void onFailure(Call<List<Transaction>> call, Throwable t) {

            }
        });
    }

    private float calculateMonthlyCumulative(List<Transaction> transactions) {
        float monthlyCummulative = 0;
        for (Transaction transaction : transactions) {
            if (transaction.getType().equals("income")) {
                monthlyCummulative += transaction.getValue();
            } else {
                monthlyCummulative -= transaction.getValue();
            }
        }
        return monthlyCummulative;
    }

    private void drawMonthsOverviewBarChart(List<BarEntry> entries) {
        // set chart x axis
        XAxis xAxis = monthsOverviewBarChart.getXAxis();
        xAxis.setGranularity(1);
        xAxis.setValueFormatter(new IntToMonthAxisValueFormatter());

        // set chart data
        MonthsOverviewBarChartDataSet set = new MonthsOverviewBarChartDataSet(entries, "Transactions cumulative");
        // set dataset colors
        set.setColors(
                ContextCompat.getColor(getBaseContext(), R.color.colorRed),
                ContextCompat.getColor(getBaseContext(), R.color.colorGreen)
        );
        BarData barData = new BarData(set);
        monthsOverviewBarChart.setData(barData);

        // set chart style
        monthsOverviewBarChart.getLegend().setEnabled(false);
        monthsOverviewBarChart.getDescription().setEnabled(false);
        monthsOverviewBarChart.setVisibleXRangeMaximum(5);
        monthsOverviewBarChart.moveViewToX(entries.get(entries.size() - 1).getX() - 3);

        // draw chart
        monthsOverviewBarChart.invalidate();
    }

    private void fillOutStatistics(List<Transaction> transactionsThisMonth) {
        int transactionCounter = 0;
        float expensesThisMonth = 0;
        float incomeThisMonth = 0;
        HashMap<String, Float> expensesByCategories = new HashMap<>();
        for (Transaction transaction : transactionsThisMonth) {
            if (transaction.getType().equals("income")) {
                incomeThisMonth += transaction.getValue();
            } else {
                expensesThisMonth += transaction.getValue();
                Category category = transaction.getCategory();
                String categoryName = "Undefined";
                if (category != null) {
                    categoryName = category.getName();
                }
                if (expensesByCategories.containsKey(categoryName)) {
                    float currentCategoryExpenses = expensesByCategories.get(categoryName);
                    expensesByCategories.put(categoryName, currentCategoryExpenses + transaction.getValue());
                } else {
                    expensesByCategories.put(categoryName, transaction.getValue());
                }
            }
            transactionCounter++;
        }
        String mostExpensiveCategory = "";
        float mostExpensiveCategoryExpenses = 0;
        for (Map.Entry<String, Float> categoryExpensesPair : expensesByCategories.entrySet()) {
            if (categoryExpensesPair.getValue() > mostExpensiveCategoryExpenses) {
                mostExpensiveCategory = categoryExpensesPair.getKey();
                mostExpensiveCategoryExpenses = categoryExpensesPair.getValue();
            }
        }

        TextView expensesNumberTextView = findViewById(R.id.expensesNumberTextView);
        TextView incomeNumberTextView = findViewById(R.id.incomeNumberTextView);
        TextView transactionsNumberTextView = findViewById(R.id.transactionsNumberTextView);
        TextView mostExpensiveCategoryTextView = findViewById(R.id.mostExpensiveCategoryTextView);

        expensesNumberTextView.setText(String.format("%.2f", expensesThisMonth));
        incomeNumberTextView.setText(String.format("%.2f", incomeThisMonth));
        transactionsNumberTextView.setText(Integer.toString(transactionCounter));
        mostExpensiveCategoryTextView.setText(mostExpensiveCategory);
    }

}
