package com.fri.tilenkopac.saveup;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button loginButton = findViewById(R.id.loginButton);
        Button transactionsButton = findViewById(R.id.transactionsButton);
        Button statisticsButton = findViewById(R.id.statisticsButton);
        Button budgetsButton = findViewById(R.id.budgetsButton);
        Button reminderButton = findViewById(R.id.reminderButton);
        FloatingActionButton newTransactionButton = findViewById(R.id.newTransactionButton);

        loginButton.setOnClickListener(v -> redirectToActivity(new LoginActivity()));
        transactionsButton.setOnClickListener(v -> redirectToActivity(new ListTransactionsActivity()));
        statisticsButton.setOnClickListener(v -> redirectToActivity(new StatisticsActivity()));
        budgetsButton.setOnClickListener(v -> redirectToActivity(new BudgetListActivity()));
        reminderButton.setOnClickListener(v -> redirectToActivity(new ReminderActivity()));
        newTransactionButton.setOnClickListener(v -> redirectToActivity(new AddTransactionActivity()));
    }

    private void redirectToActivity(Activity activity) {
        Intent intent = new Intent(this, activity.getClass());
        startActivity(intent);
    }

}