package com.fri.tilenkopac.saveup;


import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fri.tilenkopac.saveup.rest.RetrofitFactory;
import com.fri.tilenkopac.saveup.rest.entities.Location;
import com.fri.tilenkopac.saveup.rest.entities.Transaction;
import com.fri.tilenkopac.saveup.rest.services.LocationService;
import com.fri.tilenkopac.saveup.rest.services.TransactionService;
import com.fri.tilenkopac.saveup.rest.services.UserService;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

import static android.content.Context.MODE_PRIVATE;


/**
 * A simple {@link Fragment} subclass.
 */
public class GoogleMapsFragment extends Fragment implements OnMapReadyCallback {
    int transactionId;
    GoogleMap map;

    public GoogleMapsFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        //Bundle extras2 = getArguments();

        //assert extras2 != null;
        //transactionId = extras2.getInt("id");
        super.onCreate(savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_google_maps, container, false);

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Bundle extras = getArguments();

        //System.out.println("extras2: "+extras.size());
        //if(transactionId != 0){
         //   if (extras != null) {
           //     transactionId = extras.getInt("id");
            //}
        //}
        //transactionId= 13 ;//extras.getInt("id",0);
        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map1);
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        Retrofit retrofit = RetrofitFactory.getRetrofit();
        TransactionService transactionService = retrofit.create(TransactionService.class);
        map = googleMap;

        Call<Transaction> call = transactionService.getTransaction(transactionId);

        call.enqueue(new Callback<Transaction>() {
            @Override
            public void onResponse(Call<Transaction> call, Response<Transaction> response) {
                if (response.code() == 200 && response.body().getLocation() != null) {
                    Location location = response.body().getLocation();
                    LatLng markerLocation = new LatLng(location.getLatitude(), location.getLongitude());

                    MarkerOptions markerOptions = new MarkerOptions();
                    markerOptions.position(markerLocation).title(location.getAddress());
                    map.addMarker(markerOptions);
                    map.moveCamera(CameraUpdateFactory.newLatLngZoom(markerLocation, 12));
                }
            }

            @Override
            public void onFailure(Call<Transaction> call, Throwable t) {
                t.printStackTrace();

            }
        });
    }

    public void displayReceivedData(int message)
    {
        transactionId = message;
    }
}
