package com.fri.tilenkopac.saveup.rest.services;

import com.fri.tilenkopac.saveup.rest.entities.Reminder;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface ReminderService {

    @GET("reminders/{reminder_id}")
    Call<Reminder> getReminder(@Path("reminder_id") int reminderId);

}
