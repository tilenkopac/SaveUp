package com.fri.tilenkopac.saveup.rest.entities;

public class Budget {

    private int id;
    private User user;
    private String type;
    private int size;
    private int userExpenses;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public int getUserExpenses() {
        return userExpenses;
    }

    public void setUserExpenses(int userExpenses) {
        this.userExpenses = userExpenses;
    }

}
