package com.fri.tilenkopac.saveup.rest.entities;

import java.sql.Timestamp;

public class Transaction {

    private int id;
    private User user;
    private Category category;
    private Location location;
    private String type;
    private Timestamp dateAndTime;
    private String description;
    private float value;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Timestamp getDateAndTime() {
        return dateAndTime;
    }

    public void setDateAndTime(Timestamp dateAndTime) {
        this.dateAndTime = dateAndTime;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public float getValue() {
        return value;
    }

    public void setValue(float value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "Transaction{" +
                "id=" + id +
                ", user=" + user +
                ", category=" + category +
                ", location=" + location +
                ", type='" + type + '\'' +
                ", dateAndTime=" + dateAndTime +
                ", description='" + description + '\'' +
                ", value=" + value +
                '}';
    }

}
