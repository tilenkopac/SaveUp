package com.fri.tilenkopac.saveup.rest.services;

import com.fri.tilenkopac.saveup.rest.entities.Transaction;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface TransactionService {

    @GET("transactions/{transaction_id}")
    Call<Transaction> getTransaction(@Path("transaction_id") int transactionId);

}
