package com.fri.tilenkopac.saveup.rest.services;

import com.fri.tilenkopac.saveup.rest.entities.User;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface LoginService {

    @POST("login/email")
    Call<User> loginWithEmail(@Body User user);

    @POST("login/uuid")
    Call<User> loginWithUuid(@Body User user);

}
