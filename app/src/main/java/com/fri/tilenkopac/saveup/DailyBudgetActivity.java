package com.fri.tilenkopac.saveup;

import android.app.Activity;
import android.content.SharedPreferences;
import android.graphics.LightingColorFilter;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.fri.tilenkopac.saveup.rest.RetrofitFactory;
import com.fri.tilenkopac.saveup.rest.entities.Budget;
import com.fri.tilenkopac.saveup.rest.services.UserService;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by Matevz on 11. 01. 2019.
 */

public class DailyBudgetActivity extends Activity {

    TextView porabljeno;
    TextView skupno;
    TextView percentage;
    ProgressBar prg;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_daily_budget);

        porabljeno = (TextView) findViewById(R.id.spentFieldD);
        skupno = (TextView) findViewById(R.id.totalFieldD);

        prg = (ProgressBar) findViewById(R.id.progressBarDay);
        percentage = (TextView) findViewById(R.id.percentBox1);


        final SharedPreferences sharedPreferences = getSharedPreferences(SaveUpApp.SHARED_PREFERENCES_NAME, MODE_PRIVATE);
        int id = sharedPreferences.getInt(SaveUpApp.USER_ID_KEY,1);

        // GET REQUEST
        Retrofit retrofit = RetrofitFactory.getRetrofit();
        UserService userService = retrofit.create(UserService.class);
        Call<List<Budget>> call = userService.getUserBudgets(id);

        call.enqueue(new Callback<List<Budget>>() {
            @Override
            public void onResponse(Call<List<Budget>> call, Response<List<Budget>> response) {
                //System.out.println(String.format("Odgovor je: %s", response.body().toString()));
                //System.out.println(response.body().toString());
                List list;
                if (response.body() != null) {
                    list = response.body();
                } else {
                    list = new ArrayList<Budget>();
                }

                for(int i=0; i<list.size(); i++) {
                    Budget o = (Budget)list.get(i);

                    int dolocenZnesek = o.getSize();
                    String tipBudgeta = o.getType();
                    int sprotniStroski = o.getUserExpenses();

                    if(tipBudgeta.equals("daily")){
                        System.out.println("Do sedaj: "+sprotniStroski);
                        System.out.println("Od vsega: "+dolocenZnesek);
                        //TextView zapravljeno = (TextView) findViewById(R.id.spentFieldD);
                        porabljeno.setText(String.valueOf(sprotniStroski)+"          ");
                        skupno.setText(String.valueOf(dolocenZnesek)+"          ");

                        int divide = dolocenZnesek/100;

                        int procenti;
                        if (divide != 0) {
                            procenti = sprotniStroski / divide;
                        } else {
                            procenti = 0;
                        }
                        prg.setProgress(procenti);
                        percentage.setText(procenti+"%");

                        if(procenti>=90){
                            //System.out.println("Vecje od 90");
                            Drawable drawable = prg.getProgressDrawable();
                            drawable.setColorFilter(new LightingColorFilter(0x00FF00,1));
                        }else if(procenti>49){
                            //System.out.println("Vecje od 49");
                            Drawable drawable2 = prg.getProgressDrawable();
                            drawable2.setColorFilter(new LightingColorFilter(0x00FF00,1));
                        }else{
                            //System.out.println("Med 0 in 49");
                            Drawable drawable3 = prg.getProgressDrawable();
                            drawable3.setColorFilter(new LightingColorFilter(0x00FF00,1));
                        }

                    }
                }
            }

            @Override
            public void onFailure(Call<List<Budget>> call, Throwable t) {
                t.printStackTrace();
            }
        });





    }
}
