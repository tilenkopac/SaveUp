package com.fri.tilenkopac.saveup;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;

public class BudgetListActivity extends Activity {
    private Button dayButton;
    private Button weekButton;
    private Button monthButton;
    private Button addBudget;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_budget_list);

        dayButton = (Button) findViewById(R.id.dayReportButton);
        weekButton = (Button) findViewById(R.id.weekReportButton);
        monthButton = (Button) findViewById(R.id.monthReportButton);
        addBudget = (Button) findViewById(R.id.newBudgetButton);

        dayButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openDayBudget(view);
            }
        });

        weekButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openWeekBudget(view);
            }
        });

        monthButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openMonthBudget(view);
            }
        });

        addBudget.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openNewReportActivity(view);
            }
        });

    }

    public void openDayBudget(View view){
        Intent intent = new Intent(this, DailyBudgetActivity.class);
        startActivity(intent);
    }

    public void openWeekBudget(View view){
        Intent intent = new Intent(this, WeeklyBudgetActivity.class);
        startActivity(intent);
    }

    public void openMonthBudget(View view){
        Intent intent = new Intent(this, MonthlyBudgetActivity.class);
        startActivity(intent);
    }

    public void openNewReportActivity(View view){
        Intent intent = new Intent(this, NewBudgetActivity.class);
        startActivity(intent);
    }
}
