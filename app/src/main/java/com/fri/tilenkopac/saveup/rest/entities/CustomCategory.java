package com.fri.tilenkopac.saveup.rest.entities;

public class CustomCategory extends Category {

    private User user;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

}
