create table users (
	user_id int primary key auto_increment,
    user_android_id varchar(20) not null,
    user_email varchar(50) default '/',
    user_name varchar(40),
    user_last_name varchar(40),
    user_birthday date
);



create table categories (
	category_id int primary key auto_increment,
    category_name varchar(40)
);

create table user_categories (
	user_category_id int primary key auto_increment,
    user_id int,
    category_id int,
    foreign key (user_id) references users(user_id),
    foreign key (category_id) references categories(category_id)
);

/*
	Each user can have personalized categories (not the default ones).
*/

create table reminders (
	reminder_id int primary key auto_increment,
    user_id int,
    reminder_time time,
    foreign key (user_id) references users(user_id)
);
/*
	Each user can have one or more reminders.
    
	If you want to change the default output format you can convert to string with TIME_FORMAT(). E.g.:
	SELECT TIME_FORMAT(foo_hour, '%H:%i')
	FROM bar
*/
create table budgets (
	budget_id int primary key auto_increment,
    user_id int,
    budget_type varchar(20),
    budget_budget int not null,
    budget_user_expenses int not null default 0,
    foreign key (user_id) references users(user_id)
);
/*
	Each user can set daily, weekly and monthly budgets (budget_type). 
    If user expenses > budget the user must be warned. 
*/

create table transactions (
	transaction_id int primary key auto_increment,
    user_id int,
    transaction_type varchar(10) not null,
    user_category_id int,
    foreign key (user_id) references users(user_id),
    foreign key (user_category_id) references user_categories(user_category_id)
);
/*
	Each transaction has its own id, users id, what type of transaction (income, expense) 
    and transaction category (food, fitness etc.).
*/

create table locations (
	location_id int primary key auto_increment,
    transaction_id int,
    location_name varchar(50) not null,
    location_lat varchar(30),
    location_lang varchar(30),
    foreign key (transaction_id) references transactions(transaction_id)
);

/*
	Each location can have multiple transactions (User ate at the same restourant multiple times).
*/

